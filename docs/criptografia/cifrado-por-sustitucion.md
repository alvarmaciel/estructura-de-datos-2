---
sidebar_position: 1
title: Cifrado pro sustitución 
---

# Cifrado por sustitucion

En criptografía, el cifrado por sustitución es un método de cifrado por el que unidades de texto plano (cada caracter) son sustituidas con texto cifrado siguiendo un sistema regular; es decir, cada caracter es cambiado por otro siguiendo un patrón.

Uno de los cifrados por sustición más famoso es el [cifrado César](https://es.wikipedia.org/wiki/Cifrado_C%C3%A9sar)

![cifrado César](https://upload.wikimedia.org/wikipedia/commons/thumb/2/2b/Caesar3.svg/245px-Caesar3.svg.png)

El cifrado César mueve cada letra un determinado número de espacios en el alfabeto. En este ejemplo se usa un desplazamiento de tres espacios, así que una B en el texto original se convierte en una E en el texto codificado. Fijense, que en este método de cifrado, las letras X, Y, Z dan la vuelta y son reemplazadas por A, B, C respectivamente.

La regla de encripación del sifrado cesar puede ser expresada matemáticamente de la siguente forma

```python
c = (x + n) % 26
```

Donde <code>c</code> es el caracter codificado, <code>x</code> es el caracter actual a codificar y `n` es el número de posiciones que se tiene que desplazar el caracter `x`.Se hace un modulo por 26 porque son la cantidad de letras usadas en el alfabeto inglés (que es lo que vamos a usar por ahora)

<div class="alert alert-block alert-info"><b>Tip:</b> Nuestro desafío es crear un programa que pueda cifrar y descifrar textos con el Cifrado Cesar.</div>

# Primeros Pasos: Una letra

Primero vamos a estudiar dos funciones importantes que usaremos dutente el proceso de encriptado y desencriptado:

- `chr()`
- `ord()`

Es importante comprender que el alfabeto como lo conocemos, no se guarda en la memoria de la computadora como letras. Sino como números. Cada letra representa un número.
Esta es la escencia de los **DATOS**, los datos son representaciones simbólicas de un atributo o variable empirica. En nustro caso, la letra `"A"` es una representación simbólica de una serie de dígitos binarios en la computadora. Estos dígitos binarios se los denomina **bit\*** y cada letra del alfabeto se guarda en una serie de 8 bits llamados **bytes**
Por ejemplo la letra `"A"` representa el número 65, la `"B"` el 66 y así sucecivamente. Con 8 **bytes** pueden representarse 256 caracteres, y fue suficientes para casi todos los simbolos de nuestra escritura (letras: A, b; números: 1, 2, 3; o símbolos: %,&) en los primeros sistemas informáticos.
Hoy las computadoras un standar llamado **Unicode** de 16 **bytes** y en el sub conjunto **Ascii** están todas las letrasy simbolos más usados

## La función ord()

Podemos usar el método `ord()` para convertir caractéres en su representación númérica en Unicode. Esto toma ún caracter y devuelve el número representativo de la [tabla AscII](https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/ASCII-Table.svg/1261px-ASCII-Table.svg.png).

## La función chr()

Similar al método `ord()` el método `chr()` toma un número entre 0 y 254 y nos devuelve el valor de su caracter según la tabla **Ascii**

## Caracteres y números

- Lo primero que vamos a hacer es entender como funcionan las letras y las oraciones en [Python](https://www.python.org/). Para eso vamos a hacer varias cosas:

1. Escribir en una letra y mostrarla
2. Guardar una letra en una variable
3. Convertir una letra en un valor ASCII
4. Cambiar una letra por la letra siguiente

Si no tenés **Python** instalado en la computadora, podés usar el enlace de abajo para ir a una **Jupyter notebook** con la que probar:

- [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/alvarmaciel%2Festructura-de-datos-2/notebooks?labpath=%2FjupyterNotebooks%2FcifradoPorSustitucion.ipynb)

O abrir una consola de **Python Anywhere** en:

- [![python Anywhere](https://www.pythonanywhere.com/static/anywhere/images/logo-234x35.png)](https://www.pythonanywhere.com/)

## Fuentes y recursos

- [Wikipedia](https://wikipedia.org)
- [Binder](https://mybinder.org)
