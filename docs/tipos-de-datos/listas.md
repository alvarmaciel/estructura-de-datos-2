---
sidebar_position: 4
title: Listas o Iterables 
draft: True
---
Este es un resumen con actividades escrito a a partir de [List and Tuples in Python](https://realpython.com/python-lists-tuples/) y algunos capítulos de [Think Python: How to Think Like a Computer Scientist](https://greenteapress.com/thinkpython2/html/index.html)

