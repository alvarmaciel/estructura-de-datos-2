---
sidebar_position: 1
---

# Introducción

Este es el cuaderno de notas de la cursada. Acá pueden encontrar:

- Las planificaciones de clases y sus modificaciones.
- Las actividades propuestas en clases.
- Material de consulta.
- Notas sobre los temas que trabajamos.
