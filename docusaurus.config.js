const lightCodeTheme = require("prism-react-renderer/themes/github");
const darkCodeTheme = require("prism-react-renderer/themes/dracula");

// With JSDoc @type annotations, IDEs can provide config autocompletion
/** @type {import('@docusaurus/types').DocusaurusConfig} */
(
  module.exports = {
    title: "Estructura de Datos 2",
    tagline: "Escuelas Proa Villa Dolores",
    url: "https://alvarmaciel.gitlab.io/estructura-de-datos-2",
    baseUrl: "/estructura-de-datos-2/",
    onBrokenLinks: "throw",
    onBrokenMarkdownLinks: "warn",
    favicon: "img/favicon.ico",
    organizationName: "alvarmaciel", // Usually your GitHub org/user name.
    projectName: "estructura-de-datos-2", // Usually your repo name.

    presets: [
      [
        "@docusaurus/preset-classic",
        /** @type {import('@docusaurus/preset-classic').Options} */
        ({
          docs: {
            sidebarPath: require.resolve("./sidebars.js"),
            // Please change this to your repo.
            editUrl:
              "https://gitlab.com/alvarmaciel/estructura-de-datos-2/-/blob/main",
          },
          blog: {
            showReadingTime: true,
            // Please change this to your repo.
            editUrl:
              "https://gitlab.com/alvarmaciel/estructura-de-datos-2/-/blob/main",
          },
          theme: {
            customCss: require.resolve("./src/css/custom.css"),
          },
        }),
      ],
    ],

    themeConfig:
      /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
      ({
        navbar: {
          title: "Sitio de Notas",
          logo: {
            alt: "Sitio de notas Logo",
            src: "img/logo.svg",
          },
          items: [
            {
              type: "doc",
              docId: "intro",
              position: "left",
              label: "Material",
            },
            { to: "/blog", label: "Blog", position: "left" },
            {
              href: "https://gitlab.com/alvarmaciel/estructura-de-datos-2",
              label: "GitLab",
              position: "right",
            },
          ],
        },
        footer: {
          style: "dark",
          links: [
            {
              title: "Docs",
              items: [
                {
                  label: "Material",
                  to: "/docs/intro",
                },
              ],
            },
            {
              title: "Comunidad",
              items: [
                {
                  label: "Classrom",
                  href: "https://classroom.google.com",
                },
                {
                  label: "Mail",
                  href: "mailto:lamaciel@escuelasproa.edu.ar",
                },
              ],
            },
            {
              title: "Mas sobre mi",
              items: [
                {
                  label: "Blog",
                  to: "https://alvarmaciel.gitlab.io/cyberiada/",
                },
                {
                  label: "Twitter",
                  href: "https://twitter.com/alvarmaciel",
                },
                {
                  label: "GitLab",
                  href: "https://gitlab.com/alvarmaciel",
                },
              ],
            },
          ],
          copyright: `Licencia Creative Commons Atribución-NoComercial-CompartirIgual 4.0 Internacional.  Estructura de datos II. ${new Date().getFullYear()}. Hecho con Docusaurus.`,
        },
        prism: {
          theme: lightCodeTheme,
          darkTheme: darkCodeTheme,
        },
      }),
  }
);
