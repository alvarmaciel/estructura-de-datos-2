# Guardo lo que quiero cambiar en la variable mensaje
mensaje = "HOLA MUNDO"
# Creo una variable de TIPO STRING vacia para guardar el nuevo mensaje
mensaje_nuevo = ""

# Recorro la variable mensaje, que es un string. Osea UNA LISTA

for c in mensaje:  # para cada elemento (lo llamo "c") dentro de "mensaje"
    # Si c es igual a L:
    if c == "L":
        letra_nueva = "M"  # Guardo en letra_nueva la nueva letra
    elif c == "H":  # si no, cheque si c es igual a H
        letra_nueva = "Y"  # Si es igual guardo en letra:nueva Y
    else:  # Si no la letra en c no es L, ni H
        letra_nueva = c  # Guardo en letra_nueva lo que haya en c

    # mensaje_nuevo es igual a lo que haya en mensaje nuevo + la letra_nueva
    mensaje_nuevo = mensaje_nuevo + letra_nueva

# Imprimo el mensaje original
print(f"Mensaje original:  {mensaje}")
# Imprimo el menjsaje nuevo
print(f"Mensaje nuevo:  {mensaje_nuevo}")
