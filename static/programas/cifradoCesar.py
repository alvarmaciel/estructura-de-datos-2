shift = 1  # defining the shift count

texto = "HACEME UNA LLAMADA"

encryptado = ""

for c in texto:

    # chequea si el texto está en mayuscula
    if c.isupper():

        # encuentra la posición entre entre 0-25
        unicode_de_c = ord(c)

        indice_de_c = ord(c) - ord("A")

        # Hace el cambio
        nuevo_indice = (indice_de_c + shift) % 26

        # converte al nuevo caracter
        nuevo_unicode = nuevo_indice + ord("A")

        nuevo_caracter = chr(nuevo_unicode)

        # append to encrypted string
        encryptado = encryptado + nuevo_caracter

    else:

        # si no está en mayusculoa dejar todo igual
        encryptado += c

print("Texto Plano: ", texto)

print("Texto encriptado: ", encryptado)
