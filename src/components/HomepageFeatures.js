import React from 'react';
import clsx from 'clsx';
import styles from './HomepageFeatures.module.css';

const FeatureList = [
  {
    title: 'Fácil de usar',
    Svg: require('../../static/img/undraw_docusaurus_mountain.svg').default,
    description: (
      <>
        Esta página está diseñada para que sea facil de leer y
        puedan tener en un lugar los apuntes de clases.
      </>
    ),
  },
  {
    title: 'Siempre disponible',
    Svg: require('../../static/img/undraw_docusaurus_tree.svg').default,
    description: (
      <>
        El contenido que desarrollemos para las clases
        va a estar siempre disponible y para todas y todos
        quienes lo necesiten
      </>
    ),
  },
  {
    title: 'Pueden sumar sus notas',
    Svg: require('../../static/img/undraw_docusaurus_react.svg').default,
    description: (
      <>
        Es un espacio de la clase, y de quienes lo necesiten.
        Pueden enviar sus apuntes o notas para enriquecer el sitio
      </>
    ),
  },
];

function Feature({ Svg, title, description }) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} alt={title} />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
